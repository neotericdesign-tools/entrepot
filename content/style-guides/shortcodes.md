+++
title = "Shortcode Style Guide"
description = ""
publishDate = "2020-05-18"
noindex = true
+++


## Social media

#### Twitter

{{< tweet 1262389880677638157 >}}

## Video

#### Vimeo
{{< vimeo 387913227 >}}

#### YouTube
{{< youtube B6Lh-TB2_mA >}}